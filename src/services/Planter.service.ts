import { Planter } from '../entities/Planter';
import { ApiService } from './Api.service';
import { FakeData } from './FakeData';

export class PlanterService {

  private apiService = ApiService.getInstance();

  private static instance: PlanterService;

  /**
   * Singleton method to get the only one instance of the class
   */
   public static getInstance(): PlanterService {
    if (!PlanterService.instance) {
      PlanterService.instance = new PlanterService();
    }

    return PlanterService.instance;
  }

  /**
   * Get all planters
   * @returns {Planters[]} the planters
   */
  public async getAll(): Promise<Planter[]> {
    return Promise.resolve(FakeData.planters);
    // const data = await this.apiService.request<Planter[]>('get', 'planters');
    // return data.map(d => Planter.fromApi(d));
  }

  /**
   * Retrieve one entity by id
   * @param {number} id id of the entity
   */
  public async getOne(id: number): Promise<Planter> {
    // const data = await this.apiService.request<Planter>('get', `planters/${id}`);
    // return Planter.fromApi(data);
    return Promise.resolve(FakeData.planters.find(p => p.id === id)!);
  }
}