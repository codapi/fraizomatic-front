import { Planter } from '../entities/Planter';
import {TempMeasurement} from "../entities/TempMeasurement";
import {HygroMeasurement} from "../entities/HygroMeasurement";
import {BrightnessMeasurement} from "../entities/BrightnessMeasurement";
import { ApiService } from './Api.service';
import { FakeData } from './FakeData';

export class MeasurementService {

  private apiService = ApiService.getInstance();

  private static instance: MeasurementService;

  /**
   * Singleton method to get the only one instance of the class
   */
   public static getInstance(): MeasurementService {
    if (!MeasurementService.instance) {
      MeasurementService.instance = new MeasurementService();
    }

    return MeasurementService.instance;
  }

  public async getTemperatures(planter: Planter): Promise<TempMeasurement[]> {
    // const data = await this.apiService.request<TempMeasurement[]>('get', `temp_by_planter/${planter.id}`);
    // return data.map(d => TempMeasurement.fromApi(d));
    return FakeData.tempsMeasurements.filter((measurement => measurement.planter.id === planter.id));
  }

  public async getHygrometries(planter: Planter): Promise<HygroMeasurement[]> {
    // const data = await this.apiService.request<HygroMeasurement[]>('get', `hygro_by_planter/${planter.id}`);
    // return data.map(d => HygroMeasurement.fromApi(d));
    return FakeData.hygroMeasurements.filter((measurement => measurement.planter.id === planter.id));
  }
  
  public async getBrightnesses(planter: Planter): Promise<BrightnessMeasurement[]> {
    // const data = await this.apiService.request<BrightnessMeasurement[]>('get', `bright_by_planter/${planter.id}`);
    // return data.map(d => BrightnessMeasurement.fromApi(d));
    return FakeData.brightnessMeasurements.filter((measurement => measurement.planter.id === planter.id));
  }

}