import { HygroMeasurement } from '../entities/HygroMeasurement'
import { LifeCycle } from '../entities/LifeCycle'
import { Plant } from '../entities/Plant'
import { Planter } from '../entities/Planter'
import {TempMeasurement} from "../entities/TempMeasurement";
import {BrightnessMeasurement} from "../entities/BrightnessMeasurement";
import { getChangedToday } from '../utils/Utils';

export class FakeData {

  public static lifecycles = [
    new LifeCycle(100, 60, 10000, 24, 32, 2, 50, 30, 60, 180),
  ]

  public static plants = [
    new Plant('Fraisier', FakeData.lifecycles[0]),
  ]

  public static planters = [
    new Planter(1, 'Ma jardinière 1', FakeData.plants[0], false, false, 100),
    new Planter(2, 'Ma jardinière 2', FakeData.plants[0], false, false, 100),
    new Planter(3, 'Ma jardinière 3', FakeData.plants[0], false, false, 100),
  ]

  public static hygroMeasurements = [
    new HygroMeasurement(1, FakeData.planters[0], new Date(0), 20),
    new HygroMeasurement(2, FakeData.planters[0], new Date(1), 30),
    new HygroMeasurement(3, FakeData.planters[0], new Date(2), 40),
    new HygroMeasurement(4, FakeData.planters[0], new Date(3), 50),
    new HygroMeasurement(5, FakeData.planters[0], new Date(4), 60),
    new HygroMeasurement(6, FakeData.planters[0], new Date(5), 70),
  ]

  public static tempsMeasurements = [
    new TempMeasurement(1, FakeData.planters[0], new Date(0), 100),
    new TempMeasurement(2, FakeData.planters[0], new Date(2), 80),
    new TempMeasurement(3, FakeData.planters[0], new Date(4), 50),
    new TempMeasurement(4, FakeData.planters[0], new Date(5), 2),
    new TempMeasurement(5, FakeData.planters[0], new Date(8), -1.846),
    new TempMeasurement(6, FakeData.planters[0], new Date(6), -20),
  ]

  public static brightnessMeasurements = [
    new BrightnessMeasurement(1, FakeData.planters[0], getChangedToday(0), 100),
    new BrightnessMeasurement(2, FakeData.planters[0], getChangedToday(20), 200),
    new BrightnessMeasurement(3, FakeData.planters[0], getChangedToday(40), 300),
    new BrightnessMeasurement(4, FakeData.planters[0], getChangedToday(40), 500),
    new BrightnessMeasurement(5, FakeData.planters[0], new Date(1), 10000),
  ]

}