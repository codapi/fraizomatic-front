import {Measurement} from "./Measurement";
import {Planter} from "./Planter";

export class TempMeasurement extends Measurement {
    
    constructor(
        id: number,
        planter: Planter,
        datetime: Date,
        public temp: number
    ) {
        super(id, planter, datetime);
    }

    public static fromApi(tempMeas: TempMeasurement) {
      return new TempMeasurement(
        tempMeas.id,
        Planter.fromApi(tempMeas.planter),
        new Date(tempMeas.datetime),
        tempMeas.temp
      )
    }
}