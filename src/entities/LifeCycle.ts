export class LifeCycle {
  
  constructor(
    public days_before_maturity: number,
    public days_of_productivity: number,
    public sunny_hours_by_day: number,
    public optimum_mid_temp: number,
    public max_temp: number,
    public min_temp: number,
    public optimum_humidity: number,
    public min_humidity: number,
    public max_humidity: number,
    public optimum_luminosity: number
  ) {}

  public static fromApi(lifeCycle: LifeCycle): LifeCycle {
    return new LifeCycle(
      lifeCycle.days_before_maturity,
      lifeCycle.days_of_productivity,
      lifeCycle.sunny_hours_by_day,
      lifeCycle.optimum_mid_temp,
      lifeCycle.max_temp,
      lifeCycle.min_temp,
      lifeCycle.optimum_humidity,
      lifeCycle.min_humidity,
      lifeCycle.max_humidity,
      lifeCycle.optimum_luminosity,
    )
  }

}