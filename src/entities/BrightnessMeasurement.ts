import {Measurement} from "./Measurement";
import {Planter} from "./Planter";

export class BrightnessMeasurement extends Measurement {
    constructor(
        public id: number,
        public planter: Planter,
        public datetime:Date,
        public brightness: number
    ) {
        super(id, planter, datetime);
    }

    public static fromApi(brightMeas: BrightnessMeasurement): BrightnessMeasurement {
      return new BrightnessMeasurement(
        brightMeas.id,
        Planter.fromApi(brightMeas.planter),
        new Date(brightMeas.datetime),
        brightMeas.brightness,
      )
    }
}