import { StateEnum } from '../enums/StateEnum';
import { HygroMeasurement } from './HygroMeasurement';
import { LifeCycle } from './LifeCycle';

export class Plant {

  constructor(
    public name: string,
    public lifecycle: LifeCycle
  ) {}

  public static fromApi(plant: Plant): Plant {
    return new Plant(
      plant.name,
      LifeCycle.fromApi(plant.lifecycle),
    )
  }

  /**
   * Return the plant icon
   * @return {string} an svg path in the public folder
   */
  public getIcon(): string {
    return `${process.env.PUBLIC_URL}/${IconByPlant[this.name]}`;
  }
  
}

export const IconByPlant: Record<string, string> = {
  'Fraise': 'strawberry.svg',
}
