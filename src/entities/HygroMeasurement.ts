import {Measurement} from "./Measurement";
import { Planter } from './Planter';


export class HygroMeasurement extends Measurement {
    constructor(
        id: number,
        planter: Planter,
        datetime: Date,
        public hygrometry: number,
    ) {
        super(id, planter, datetime);
    }

    public static fromApi(hygroMeas: HygroMeasurement) {
      return new HygroMeasurement(
        hygroMeas.id,
        Planter.fromApi(hygroMeas.planter),
        new Date(hygroMeas.datetime),
        hygroMeas.hygrometry,
      )
    }
}