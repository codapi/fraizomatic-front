import { StateEnum } from '../enums/StateEnum';
import { MeasurementService } from '../services/Measurement.service';
import { isToday } from '../utils/Utils';
import { HygroMeasurement } from './HygroMeasurement';
import { Plant } from './Plant'
import { TempMeasurement } from './TempMeasurement';

export class Planter {

  measurementService = MeasurementService.getInstance();
  
  constructor(
    public id: number,
    public name: string,
    public plant: Plant,
    public is_light_switched_on: boolean,
    public is_solenoid_valve_opened: boolean,
    public water_level: number,
  ) {}

  public static fromApi(planter: Planter): Planter {
    return new Planter(
      planter.id,
      planter.name,
      Plant.fromApi(planter.plant),
      planter.is_light_switched_on,
      planter.is_solenoid_valve_opened,
      planter.water_level,
    )
  }

  /**
   * Get the corresponding StateEnum of the solenoid valve state
   */
  public getSolenoidValveState(): StateEnum {
    return this.is_solenoid_valve_opened ? StateEnum.Good : StateEnum.Disabled;
  }

  /**
   * Get the state of the hygrometry value
   */
   public getHygroState(hygroMeas: HygroMeasurement): StateEnum {
    if (hygroMeas.hygrometry > this.plant.lifecycle.max_humidity) return StateEnum.PositiveDanger;
    if (hygroMeas.hygrometry < this.plant.lifecycle.min_humidity) return StateEnum.NegativeDanger;
    return StateEnum.Good;
  }

  /**
   * Get the state of the temp value
   */
   public getTempState(tempMeas: TempMeasurement): StateEnum {
    if (tempMeas.temp > this.plant.lifecycle.max_temp) return StateEnum.PositiveDanger;
    if (tempMeas.temp < this.plant.lifecycle.min_temp) return StateEnum.NegativeDanger;
    return StateEnum.Good;
  }

    /**
    * Get the state of the light value
    */
    public getLightState(): StateEnum {
      return this.is_light_switched_on ? StateEnum.Good : StateEnum.Disabled;
    }

  /**
   * Get the accumulated light for today in percent.
   */
  public async getPercentAccumulatedLuxADay(): Promise<number> {
    const brightMeas = await this.measurementService.getBrightnesses(this);
    const measOfDayArray = brightMeas.filter(m => isToday(m.datetime));
    if (measOfDayArray.length) {
      const luxSum = measOfDayArray.map((m) => m.brightness).reduce((a, b) => a + b);
      return luxSum * 100 / this.plant.lifecycle.sunny_hours_by_day;
    }

    return 0;
  }

}