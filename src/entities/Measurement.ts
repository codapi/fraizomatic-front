import {Planter} from "./Planter";

export class Measurement {
    constructor(
        public id: number,
        public planter: Planter,
        public datetime:Date
    ) {}
}