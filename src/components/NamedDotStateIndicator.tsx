import React from 'react';
import { StateEnum } from '../enums/StateEnum';
import { Image } from 'antd';

import '../styles/CustomCard.scss';
import '../styles/NamedDotStateIndicator.scss';
import { DotStateIndicator, IStateNameMap } from './DotStateIndicator';

interface IProps {
  state: StateEnum,
  stateNameMap: IStateNameMap,
  icon: string,
  name: string,
}

export const NamedDotStateIndicator: React.FC<IProps> = (props: IProps) => {

  return (
    <div className="named-dot-state-indicator">
      <div className='name-container'>
        <Image preview={false} src={props.icon} />
        <p>{props.name}</p>
      </div>
      <DotStateIndicator {...props} />
    </div>
  )
}