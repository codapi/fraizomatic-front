import React from "react";
import {Image, Layout} from "antd";
import {NavLink} from "react-router-dom";
const {Header} = Layout;

interface IProps {
}

const AppHeader: React.FC<IProps> = () => {
    return (
        <Header className="header">
            <Image
                width={48}
                src={`${process.env.PUBLIC_URL}/logo367.png`}
                preview={false}
            />
            <NavLink to="/"><h1 className="header-title">Fraiz’omatic</h1></NavLink>
            {/* <div>Des fraises pour garder la banane.</div> */}
        </Header>
    )
}

export default AppHeader;