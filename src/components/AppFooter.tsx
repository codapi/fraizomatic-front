import React from "react";
import {Layout} from "antd";
import {NavLink} from "react-router-dom";

const {Footer} = Layout;

interface IProps {
}

const AppFooter: React.FC<IProps> = () => {
    return (
        <Footer className="footer" style={{textAlign: 'center'}}>
            <NavLink to="/conditions-generales">Conditions générales</NavLink>
            <NavLink to="/droit-a-l-oublie">Droit à l'oublie</NavLink>
            <NavLink to="/contacts">Contactes</NavLink>
        </Footer>
    )
}

export default AppFooter;