import { Image, Slider } from 'antd';
import { SliderMarks } from 'antd/lib/slider';
import React from 'react';

import '../styles/BarValueIndicator.scss';

interface IProps {
  label: string
  value: number
  unit: string
  min: number
  max: number
  gap?: number
  extreme?: {
    min: number,
    max: number,
  }
  icon: string
}

export const BarValueIndicator: React.FC<IProps> = (props: IProps) => {

  /**
   * Get slider marks for the AntDesign Slider components.
   * Generate them from props.
   * @returns {SliderMarks} Slider antd marks value
   */
  function getMarks(): SliderMarks {
    const extremeMin = props.gap ? (props.min - props.gap) : props.extreme!.min;
    const extremeMax = props.gap ? (props.max + props.gap) : props.extreme!.max;

    return {
      [extremeMin]: `${extremeMin} ${props.unit}`,
      [props.value]: {
        style: {
          color: '#000',
          fontWeight: 'bold',
        },
        label: `${props.value} ${props.unit}`
      },
      [extremeMax]: `${extremeMax} ${props.unit}`,
    }
  }

  function getMin(): number {
    return props.gap ? (props.min - props.gap) : props.extreme!.min;
  }

  function getMax(): number {
    return props.gap ? (props.max + props.gap) : props.extreme!.max;
  }

  return (
    <div className="bar-value-indicator">
      <h4 className="bvi-label">{props.label}</h4>
      <div className="bar-container">
        <Image preview={false} src={props.icon} />
        <Slider
          range
          marks={getMarks()}
          included={true}
          defaultValue={[props.min, props.max]}
          min={getMin()}
          max={getMax()}
          disabled
        />
      </div>
    </div>
  )
}