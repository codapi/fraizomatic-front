import React, { useEffect } from 'react';
import { Plant } from '../entities/Plant';
import { Image } from 'antd';

import '../styles/PlantCard.scss';

interface IProps {
  plant: Plant
}

export const PlantCard: React.FC<IProps> = (props: IProps) => {

  return (
    <div className="plant-card">
      <Image preview={false} src={props.plant.getIcon()} />
      <p>{props.plant.name}</p>
    </div>
  )
}