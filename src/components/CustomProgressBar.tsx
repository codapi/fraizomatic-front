import { Progress, Image } from 'antd';
import React from 'react';

import '../styles/CustomProgressBar.scss';

interface IProps {
  name: string,
  percent: number,
  icon: string,
}

export const CustomProgressBar: React.FC<IProps> = (props: IProps) => {

  return (
    <div className="custom-progress-bar">
      <p>{props.name}</p>
      <div className="progress-container">
        <Image preview={false} src={props.icon} />
        <Progress percent={props.percent} strokeColor="#75ED88" />
      </div>
    </div>
  )
}