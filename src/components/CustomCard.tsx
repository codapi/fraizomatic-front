import React, { CSSProperties, ReactElement } from 'react';

import '../styles/CustomCard.scss';

interface IProps {
  title: string,
  headerContent?: ReactElement,
  bodyContent?: ReactElement,
  style?: CSSProperties;
}

export const CustomCard: React.FC<IProps> = (props: IProps) => {

  return (
    <div className="custom-card" style={props.style || {}}>
      <div className="custom-card-header">
        <h3 className="custom-card-title">{props.title}</h3>
        {props.headerContent}
      </div>
      <div className="custom-card-body">
        {props.bodyContent}
      </div>
    </div>
  )
}