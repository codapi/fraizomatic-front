import React from 'react';
import Layout, { Content } from 'antd/lib/layout/layout';
import AppHeader from './AppHeader';

import '../styles/AppLayout.scss';

interface IProps {
  children: React.ReactElement,
}

export const AppLayout: React.FC<IProps> = (props: IProps) => {

  return (
    <Layout className="app-layout">
      <AppHeader />
      <Content className="content">
        {props.children}
      </Content>
      {/* <AppFooter /> */}
    </Layout>
  )
}