import React from 'react';
import { StateEnum, StateEnumColor } from '../enums/StateEnum';

import '../styles/DotStateIndicator.scss';

export interface IStateName {
  state: StateEnum,
  name: string,
}
export interface IStateNameMap extends Array<IStateName> {}

interface IProps {
  state: StateEnum,
  stateNameMap: IStateNameMap,
}

export const DotStateIndicator: React.FC<IProps> = (props: IProps) => {

  /**
   * Get the label associated with the current state
   * @returns {string} the label
   */
  function getLabel(): string {
    return props.stateNameMap.find(el => el.state === props.state)!.name;
  }

  /**
   * Get the color associated with the current state
   * @returns {string} the color
   */
  function getColor(): string {
    return StateEnumColor[props.state];
  }

  return (
    <div className="dot-state-indicator">
      <div className="dsi-dot" style={{ background: getColor() }}></div>
      <p>{getLabel()}</p>
    </div>
  )
}