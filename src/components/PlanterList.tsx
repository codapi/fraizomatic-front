import { List, Image } from 'antd';
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Planter } from '../entities/Planter';
import { PlanterService } from '../services/Planter.service';

import '../styles/PlanterList.scss';

interface IProps {

}

export const PlanterList: React.FC<IProps> = (props: IProps) => {

  const [planters, setPlanters] = useState<Planter[]>([]);

  const planterService = PlanterService.getInstance();

  const history = useHistory();

  /**
   * Retrieve all the planters for the list
   */
  async function getData(): Promise<void> {
    const retrieved = await planterService.getAll();
    setPlanters(retrieved);
  }

  /**
   * Handle the click event of a item of the list
   * @param {Planter} planter clicked item data
   */
  function handleOnItemClick(planter: Planter): void {
    history.push(`/planter-detail/${planter.id}`)
  }

  useEffect(() => {
    getData();
  }, []);
  
  return (
    <List
      className="planter-list"
      itemLayout="horizontal"
      dataSource={planters}
      renderItem={item => (
        <List.Item
          className="planter-list-item"
          onClick={ () => handleOnItemClick(item)}
        >
          <Image src="strawberry.svg" preview={false} />
          <p>{item.name}</p>
        </List.Item>
      )}
    />
  )
}