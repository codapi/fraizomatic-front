import React, {useEffect, useState} from "react";
import Chart from "react-apexcharts";
import {Planter} from "../entities/Planter";
import {MeasurementService} from "../services/Measurement.service";

interface IProps {
  planter: Planter
}

export const MeasurementChart: React.FC<IProps> = (props: IProps) => {

  const [axis, setAxis] = useState<any>([]);
  const [series, setSeries] = useState<any>([]);

  const measurementService = MeasurementService.getInstance();

  async function getData(): Promise<void> {
    const temperatures = await measurementService.getTemperatures(props.planter);
    const hygro = await measurementService.getHygrometries(props.planter);
    const brightness = await measurementService.getBrightnesses(props.planter);

    const axis = {
      type: 'datetime',
      labels: {
        maxWidth: 200,
        formatter: (e: string) => {
          const d: Date = new Date(e)
          console.log(" X value : ")
          console.log(e);
          const hours = d.getHours() < 10 ? `0${d.getHours()}` : d.getHours();
          const minutes = d.getMinutes() < 10 ? `0${d.getMinutes()}` : d.getMinutes();
          const day = d.getDate() < 10 ? `0${d.getDate()}` : d.getDate();
          const month = d.getMonth()+1 < 10 ? `0${d.getMonth()+1}` : d.getMonth()+1;
          const year = d.getFullYear() < 10 ? `0${d.getFullYear()}` : d.getFullYear();
          return `${day}/${month}/${year} ${hours}:${minutes}`;
        },
      }
    }
    setAxis(axis);

    const series = [
      {name: 'Température', data: temperatures.map(temp => {return {x: temp.datetime.toString(), y: temp.temp}})},
      {name: 'Hygrométrie', data: hygro.map(hyg => {return {x: hyg.datetime.toString(), y: hyg.hygrometry}})},
      {name: 'Luminosité', data: brightness.map(bright => {return {x: bright.datetime.toString(), y: bright.brightness}})},
    ]
    console.log(series);
    setSeries(series);
  }

  useEffect(() => {
    getData();
  }, []);

  const options = {
      chart: {
        id: "lineChart"
      },
      xaxis: {...axis, tooltip: {enabled: false}},
      responsive: [
        {
          breakpoint: 800,
          options: {
            legend: {
              position: "bottom"
            }
          }
        }
      ]
  };

  return (
    <Chart
      options={options}
      series={series}
      type="line"
      width="80%"
      height="auto"

    />
  )
}