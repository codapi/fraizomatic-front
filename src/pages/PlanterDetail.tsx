import { Spin } from 'antd';
import React, { useEffect, useState } from "react";
import { useParams } from 'react-router';
import { BarValueIndicator } from '../components/BarValueIndicator';
import { CustomCard } from '../components/CustomCard';
import { CustomProgressBar } from '../components/CustomProgressBar';
import { DotStateIndicator } from '../components/DotStateIndicator';
import { NamedDotStateIndicator } from '../components/NamedDotStateIndicator';
import { PlantCard } from '../components/PlantCard';
import Space from '../components/Space';
import { Planter } from '../entities/Planter';
import { StateEnum } from '../enums/StateEnum';
import { MeasurementService } from '../services/Measurement.service';
import { PlanterService } from '../services/Planter.service';
import '../styles/PlanterDetail.scss';
import {MeasurementChart} from "../components/MeasurementChart";

export interface BVIPlanterData {
  value: number,
  min: number,
  max: number,
  state: StateEnum,
}

interface IProps {

}

export const PlanterDetail: React.FC<IProps> = () => {

  const [planter, setPlanter] = useState<Planter | undefined>();
  const [hygroBVIData, setHygroBVIData] = useState<BVIPlanterData>();
  const [tempBVIData, setTempBVIData] = useState<BVIPlanterData>();
  const [luxPercent, setLuxPercent] = useState<number>(0);

  const planterService = PlanterService.getInstance();
  const measurementService = MeasurementService.getInstance();

  const params = useParams<{id: string}>();

  /**
   * Get the planter from id passed in params.
   * Set the corresponding state.
   */
  async function getPlanter(): Promise<void> {
    const id = Number(params.id);
    const retrieved = await planterService.getOne(id);
    debugger;
    setPlanter(retrieved);
  }

  /**
   * Get the data of the Hygrometry BarValueIndicator.
   */
  async function getHygroBVIData(planter: Planter): Promise<BVIPlanterData> {
    const planterHygroMeasurements = await measurementService.getHygrometries(planter);
    const latest = planterHygroMeasurements.sort((a, b) => {
      return new Date(b.datetime).getTime() - new Date(a.datetime).getTime();
    })[0];
    
    const data = {
      value: latest.hygrometry,
      min: planter.plant.lifecycle.min_humidity,
      max: planter.plant.lifecycle.max_humidity,
      state: planter.getHygroState(latest),
    }
    debugger;
    return data;
  }

  /**
   * Get the data of the Hygrometry BarValueIndicator.
   */
  async function getTempBVIData(planter: Planter): Promise<BVIPlanterData> {
    const planterTempMeasurements = await measurementService.getTemperatures(planter);
    const latest = planterTempMeasurements.sort((a, b) => {
      return new Date(b.datetime).getTime() - new Date(a.datetime).getTime();
    })[0];
    
    return {
      value: latest.temp,
      min: planter.plant.lifecycle.min_temp,
      max: planter.plant.lifecycle.max_temp,
      state: planter.getTempState(latest),
    }
  }

  /**
   * Load all data of the page.
   * @param {Planter} planter planter to get the data from. 
   */
  async function loadData(planter: Planter): Promise<void> {
    setHygroBVIData(await getHygroBVIData(planter));
    setTempBVIData(await getTempBVIData(planter));
    setLuxPercent(await planter.getPercentAccumulatedLuxADay());
  }

  useEffect(() => { getPlanter() }, [])

  useEffect(() => {
    if (planter) {
      loadData(planter);
    }
  }, [planter])

  return (
    <div className="planter-detail">
      {planter && <div style={{ width: "100%", height: "100%" }}>
        <h1>{planter.name}</h1>

        {/* Plant */}
        <CustomCard
          title='La plante'
          style={{ marginBottom: 16 }}
          headerContent={
            <div style={{ display: 'flex', alignItems: 'center', flexDirection: 'column' }}>
              <Space height={8} />
              <PlantCard plant={planter.plant} />
            </div>
          }
          bodyContent={
            <div>
              {hygroBVIData && <BarValueIndicator
                label={"Humidité de la terre"}
                value={hygroBVIData?.value}
                unit={'%'}
                min={hygroBVIData.min}
                max={hygroBVIData.max}
                // gap={20}
                extreme={{ min: 0, max: 100 }}
                icon={`${process.env.PUBLIC_URL}/drop.svg`}
              />}

              {hygroBVIData && <DotStateIndicator
                state={hygroBVIData.state}
                stateNameMap={[
                  { state: StateEnum.NegativeDanger, name: 'Trop sèche' },
                  { state: StateEnum.PositiveDanger, name: 'Trop humide' },
                  { state: StateEnum.Good, name: 'Parfait' }
                ]}
              />}

              <Space height={48} />

             {planter && <NamedDotStateIndicator
                name="Arrosage"
                icon={`${process.env.PUBLIC_URL}/water-tap.svg`}
                state={planter.getSolenoidValveState()}
                stateNameMap={[
                  { state: StateEnum.Disabled, name: 'Désactivé' },
                  { state: StateEnum.Good, name: 'Activé' }
                ]}
              />}

            </div>
          }
        />

        <CustomCard
          title="La Jardinière"
          bodyContent={
            <div>
              {tempBVIData && <BarValueIndicator
                label={"Température extérieur"}
                value={tempBVIData.value}
                unit={'C°'}
                min={tempBVIData.min}
                max={tempBVIData.max}
                gap={10}
                icon={`${process.env.PUBLIC_URL}/thermometer.svg`}
              />}

              {tempBVIData && <DotStateIndicator
                state={tempBVIData.state}
                stateNameMap={[
                  { state: StateEnum.NegativeDanger, name: 'Trop froid' },
                  { state: StateEnum.PositiveDanger, name: 'Trop chaud' },
                  { state: StateEnum.Good, name: 'Parfait' }
                ]}
              />}
  
              <Space height={16} />

              <CustomProgressBar
                name="Luminosité accumulée"
                percent={luxPercent}
                icon={`${process.env.PUBLIC_URL}/idea.svg`}
              />

              <Space height={16} />

              {planter && <NamedDotStateIndicator
                name="Lampe UV"
                icon={`${process.env.PUBLIC_URL}/uv-lamp.svg`}
                state={planter.getLightState()}
                stateNameMap={[
                  { state: StateEnum.Disabled, name: 'Éteinte' },
                  { state: StateEnum.Good, name: 'Allumée' }
                ]}
              />}
            </div>
            
          }
        />

        <Space height={8} />

        {planter && <MeasurementChart planter={planter}/>}
      </div>}
      {!planter && <div className="spin-container"><Spin /></div>}
    </div>
  )
}