import {Button} from 'antd';
import React from 'react';
import {NavLink} from 'react-router-dom'
import { PlanterList } from '../components/PlanterList';
import '../styles/Home.scss';


interface IProps {
}

const Home: React.FC<IProps> = () => {

    return (
        <div className='home'>
            <div className="home-buttons">
                {/* <NavLink to="/inscription"><Button type="primary">Inscription</Button></NavLink> */}
            </div>
            <PlanterList />
            { <NavLink to="/cgu"><Button type="primary">Conditions d'utilisation</Button></NavLink>}
        </div>
    )
}

export default Home;