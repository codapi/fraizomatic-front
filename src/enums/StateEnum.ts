export enum StateEnum {
  Disabled,
  NegativeDanger,
  PositiveDanger,
  Good,
}

export const StateEnumColor = {
  [StateEnum.Disabled]:         'lightgrey',
  [StateEnum.NegativeDanger]:   'red',
  [StateEnum.PositiveDanger]:   'red',
  [StateEnum.Good]:             'green',

}