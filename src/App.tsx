import React from 'react';
import './App.css';
import Home from "./pages/Home";
import {BrowserRouter, Switch, Route} from "react-router-dom";
import Connexion from "./pages/Connexion";
import Inscription from "./pages/Inscription";
import { AppLayout } from './components/AppLayout';
import { PlanterDetail } from './pages/PlanterDetail';
import CGU from "./pages/ConditionsUtilisation";


function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <AppLayout>
          <Switch>
            <Route path="/" exact component={() => <Home/>}/>
            <Route path="/connexion" exact component={() => <Connexion/>}/>
            <Route path="/inscription" exact component={() => <Inscription/>}/>
            <Route path="/planter-detail/:id" component={() => <PlanterDetail />}/>
            <Route path="/cgu" component={() => <CGU />}/>
            <Route path="*" component={() => <Home/>}/>
          </Switch>
        </AppLayout>
      
      </BrowserRouter>
    </div>
    );
  }
  
  export default App;
  